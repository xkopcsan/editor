#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "main.h"

bool isValidChar(char ch) {
    return ch!='\n' && ch!=EOF && ch!='\0';
}

void deleteNewLine(char *line) {
    size_t i = strlen(line) - 1;
    if (line[i] == '\n')
        line[i] = '\0';
}

void readAllLines(char lines[128][1024], int *counter, FILE *input) {

    char line[1024];
    while (fgets(line, sizeof(line), input)) {
        strcpy(lines[*counter], line);
        *counter += 1;
    }

}

int readNumber(char *args) {
    char *trash;
    return (int) strtol(args, &trash, 10);
}

void empty(char *text_line) {
    int i = 0;
    while (text_line[i] != '\0') {
        text_line[i] = '\0';
        i++;
    }
}

void prepend(char *src, char *dest) {
    char tmp[strlen(dest)];
    empty(tmp);
    //strcat = append
    strcat(tmp, src);
    strcat(tmp, dest);
    strcpy(dest, tmp);
}

bool isInstruction(char instruction) {
    size_t len = sizeof(instructions) / sizeof(char);
    for (size_t i = 0; i < len; ++i) {
        if (instructions[i] == instruction) {
            return true;
        }
    }
    return false;
}

void doInsert(char *src, char *dest) {
    prepend(src, dest);
}

void doPrepend(char *src, char *dest) {
    deleteNewLine(src);
    prepend(src, dest);
}

void doAppend(char *src, char *dest) {
    deleteNewLine(dest);
    strcat(dest, src);
}

void doDelete(char *args, int *line) {
    int repeats = 1;
    if (strlen(args) != 0) {
        repeats = readNumber(args);
    }
    for (int i = 0; i < repeats; i++) {
        *line += 1;
    }
}

void doRemoveEOL(char *dest) {
    deleteNewLine(dest);
}

void doNext(char text[128][1024], char *instruction_args, int *line) {
    deleteNewLine(instruction_args);
    int repeats = 1;
    if (strlen(instruction_args)!=0) {
        repeats = readNumber(instruction_args);
    }
    for (long i = 0; i < repeats; i++) {
        printf("%s", text[*line]);
        *line+=1;
    }
}

void doGoTo(char *args, int *instruction_line) {
    *instruction_line = readNumber(args) - 2;
}

void doInstruction(char *instruction_with_args, char text[128][1024], int *instruction_line, int *text_line) {
    char instruction = instruction_with_args[0];
    char *args = (instruction_with_args + 1);
    switch (instruction) {
        case 'i':
            doInsert(args, text[*text_line]);
            break;
        case 'b':
            doPrepend(args, text[*text_line]);
            break;
        case 'a':
            doAppend(args, text[*text_line]);
            break;
        case 'd':
            doDelete(args, text_line);
        case 'r':
            doRemoveEOL(text[*text_line]);
        case 'n':
            doNext(text, args, text_line);
            break;
        case 'g':
            doGoTo(args, instruction_line);
            break;
        default:
            break;
    }
}

void readRest(char lines[128][1024], int line) {

    while (lines[line][0]!='\0') {
        printf("%s", *(lines + line));
        line++;
    }

}

void writeHelp() {
    printf("Zle zadaný príkaz\nProgram potrebuje vstupný súbor");
}

int main(int argc, char *argv[]) {

    //ak je zly pocet argumentov, vypise chybu a skonci program
    if (argc!=2) {
        writeHelp();
        return 1;
    }

    //array stringov instrukcii
    char my_instructions[128][1024];
    //pocet instrukcii
    int number_of_instructions = 0;
    //nacitanie suboru do <my_instructions>
    FILE *instructions_file = fopen(argv[1], "r");
    readAllLines(my_instructions, &number_of_instructions, instructions_file);

    //array stringov riadkov textu
    char my_text[128][1024];
    //pocet riadkov textu
    int number_of_lines = 0;
    //nacitanie vstupu do my_text
    readAllLines(my_text, &number_of_lines, stdin);

    //sucasne riadky instrukcii, textu
    int instruction_line = 0;
    int text_line = 0;

    //kym riadok instrukcie je mensi ako pocet riadkov instrukcii
    //a kym riadok textu je mensi ako pocet riadkov textu
    while (instruction_line<number_of_instructions &&
            text_line < number_of_lines) {
        // && my_instructions[instruction_line][0] != '\0'

        //instrukcia s argumentami
        char *instruction_with_args = my_instructions[instruction_line];

        //vytiahnuta instrukcia z <instruction_with_args> (prvy char)
        char instruction = instruction_with_args[0];

        //ak char nie je validny, vypisat chybu a skoncit program
        if (isValidChar(instruction) && !isInstruction(instruction)) {
            printf("Príkaz %c neexistuje\n", instruction);
            return 1;
        }

        //vykonat instrukciu
        doInstruction(instruction_with_args, my_text, &instruction_line, &text_line);

        instruction_line++;
    }

    readRest(my_text, text_line);

    fclose(instructions_file);

    return 0;

}