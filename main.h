#ifndef editor
#define editor

const char instructions[] = { 'i', 'b', 'a', 'd', 'r', 's', 'S', 'n', 'q', 'g', 'f', 'c', 'e' };

bool isValidChar(char ch);

//funkcia na zmazanie \n z konca passnuteho stringu
void deleteNewLine(char *line);

//funkcia na precitanie vsetkych riadkov zo suboru a ulozenie do arrayu stringov
void readAllLines(char lines[128][1024], int *counter, FILE *input);

//funkcia na precitanie integeru zo stringu
int readNumber(char *args);

//funkcia na zmazanie pripadneho trashu z arrayu charov
void empty(char *text_line);

//funkcia, ktore pred <dest> pripoji <src>
void prepend(char *src, char *dest);

//funkcia, ktora zisti ci <instruction> je validna instrukcia
bool isInstruction(char instruction);

//funkcia, ktora pred <dest> pripoji <src> aj s novym riadkom
void doInsert(char *src, char *dest);

//funkcia, ktora pred <dest> pripoji <src> bez noveho riadku
void doPrepend(char *src, char *dest);

//za <dest> pripoji <src>
void doAppend(char *src, char *dest);

//funkcia, ktora preskoci <x> riadkov, kde <x> je integer z argumentu instrukcie
void doDelete(char *args, int *line);

//zmaze \n
void doRemoveEOL(char *dest);

//funkcia, ktora vypise <x> riadkov a posunie sa o <x> riadkov dalej
//<x> je integer z argumentu funkcie
void doNext(char text[128][1024], char *instruction_args, int *line);

//funkcia, ktora skoci na <instruction_line> instrukciu
void doGoTo(char *args, int *instruction_line);

//funkcia, ktora precita a vykona zadanu instrukciu
void doInstruction(char *instruction_with_args, char text[128][1024], int *instruction_line, int *text_line);

//funkcia na precitanie nespracovaneho textu
void readRest(char lines[128][1024], int line);

//funkcia na vypisanie spravneho pouzivania
void writeHelp();

#endif //editor
